(function($){
    $(document).ready(function(){
        $('.select-all').change(function(){
            var select = $(this).closest('.products').find('.check .select-product');

            if($(this).prop('checked')){
                select.prop('checked',true);
            }else{
                select.prop('checked',false);
            }
        });
     });
})(jQuery);